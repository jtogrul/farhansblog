from django import template
from semistatic import models
from semistatic import exceptions

register = template.Library()


@register.assignment_tag
def semistatic_content(content_label):
    content = models.Content.objects.get(label=content_label)
    return responsify_content(content)


@register.assignment_tag
def semistatic_content_list(content_list_name):
    content_list = models.ContentList.objects.get(name=content_list_name).content_set.all()

    response = []

    for content in content_list:
        response.append(responsify_content(content))

    return response


@register.simple_tag
def semistatic_field(contentpart_map, part_label):
    if part_label in contentpart_map.keys():
        return contentpart_map[part_label].get_content()
    else:
        raise exceptions.ContentPartNotFound("Content field '%s' does not exist " % part_label)


def responsify_content(content):
    response = {}

    for content_part in content.contentpart_set.all():
        response[content_part.label] = content_part

    return response