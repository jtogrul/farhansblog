from django.apps import AppConfig


class SemistaticConfig(AppConfig):
    name = 'semistatic'
