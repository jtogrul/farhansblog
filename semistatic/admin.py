from django.contrib import admin
from polymorphic.admin import PolymorphicInlineSupportMixin, StackedPolymorphicInline
from semistatic.models import *


class ContentPartInline(StackedPolymorphicInline):
    class TextContentPartInline(StackedPolymorphicInline.Child):
        model = TextContentPart

    class CharContentPartInline(StackedPolymorphicInline.Child):
        model = CharContentPart

    class ImageContentPartInline(StackedPolymorphicInline.Child):
        model = ImageContentPart

    model = ContentPart
    child_inlines = (
        TextContentPartInline,
        CharContentPartInline,
        ImageContentPartInline,
    )


class ContentAdmin(PolymorphicInlineSupportMixin, admin.ModelAdmin):
    inlines = (ContentPartInline,)
    ordering = ('order',)

admin.site.register(ContentList)
admin.site.register(Content, ContentAdmin)
