from django.db import models
from polymorphic.models import PolymorphicModel


class ContentList(models.Model):
    name = models.CharField(max_length=100)
    desc = models.TextField(max_length=500)

    def __str__(self):
        return self.name


class Content(models.Model):
    list = models.ForeignKey(ContentList, blank=True, null=True)
    label = models.CharField(max_length=100, unique=True, null=True, blank=True)
    desc = models.TextField()
    order = models.IntegerField(default=0)

    def __str__(self):
        return self.label


class ContentPart(PolymorphicModel):
    content = models.ForeignKey(Content)
    label = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.label


class TextContentPart(ContentPart):
    text = models.TextField()

    def get_content(self):
        return self.text


class CharContentPart(ContentPart):
    char = models.CharField(max_length=256)

    def get_content(self):
        return self.char


class ImageContentPart(ContentPart):
    image = models.ImageField(blank=True, upload_to='uploaded_photos') # TODO get directory from settings

    def get_content(self):
        return self.image.url
