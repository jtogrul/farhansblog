from django.shortcuts import render, redirect, get_object_or_404
from django.conf import settings
from . import models
import stripe
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def portfolio(request):
    page = request.GET.get('page')
    theme_id = request.GET.get('theme')

    try:
        theme = models.PortfolioTheme.objects.get(id=theme_id)
    except models.PortfolioTheme.DoesNotExist:
        theme_id = None

    if theme_id:
        photos_all = models.PortfolioPhoto.objects.filter(theme_id=theme_id).order_by('-id').all()
    else:
        photos_all = models.PortfolioPhoto.objects.order_by('-id').all()

    themes = models.PortfolioTheme.objects.order_by('name').all()

    paginator = Paginator(photos_all, 12)

    try:
        photos = paginator.page(page)
        photos.paginator
    except PageNotAnInteger:
        photos = paginator.page(1)
    except EmptyPage:
        photos = paginator.page(paginator.num_pages)

    return render(request, 'portfolio.html', {'photos': photos, 'themes': themes, 'theme_id': theme_id})


def home(request):
    slides = models.Slide.objects.all()
    return render(request, 'home.html', {'slides': slides})


def projects(request):
    page = request.GET.get('page')
    projects_all = models.Project.objects.order_by('-id').all()

    paginator = Paginator(projects_all, 8)

    try:
        projects = paginator.page(page)
    except PageNotAnInteger:
        projects = paginator.page(1)
    except EmptyPage:
        projects = paginator.page(paginator.num_pages)

    return render(request, 'projects.html', {'projects': projects})


def project(request, id):
    the_project = get_object_or_404(models.Project, pk=id)
    newer = models.Project.objects.filter(id__gt=id).order_by('-id').first()
    older = models.Project.objects.filter(id__lt=id).order_by('id').first()
    if not newer:
        newer = models.Project.objects.order_by('?').first()
    if not older:
        older = models.Project.objects.order_by('?').first()

    return render(request, 'project.html', {'project': the_project, 'newer': newer, 'older': older})


def store(request):
    return render(request, 'store.html')


def about(request):
    footer_photos = models.PortfolioPhoto.objects.all().order_by('-id')[:10]
    about_slides = models.AboutSlide.objects.all().order_by('order')
    return render(request, 'about.html', {'footer_photos': footer_photos, 'about_slides': about_slides})


def contact(request):

    if request.method == 'GET':
        return render(request, 'contact.html')
    elif request.method == 'POST':
        email = request.POST['email']
        name = request.POST['name']
        subject = request.POST['subject']
        message = request.POST['message']

        args = {'email': email, 'name': name, 'subject': subject, 'message': message}

        html_content = render_to_string('email-contact.html', args)  # ...
        text_content = strip_tags(html_content)  # this strips the html, so people will have the text as well.

        # create the email, and attach the HTML version as well.
        msg = EmailMultiAlternatives(subject, text_content, email, [settings.ADMIN_MAIL])
        msg.attach_alternative(html_content, "text/html")

        try:
            msg.send()
            return render(request, 'contact.html', {'mail_sent': 'ok'})
        except:
            return render(request, 'contact.html', {'mail_sent': 'error'})


def services(request):
    return render(request, 'services.html')


def blog(request):
    page = request.GET.get('page')
    blog_posts_all = models.BlogPost.objects.all()
    paginator = Paginator(blog_posts_all, 12)

    try:
        blog_posts = paginator.page(page)
    except PageNotAnInteger:
        blog_posts = paginator.page(1)
    except EmptyPage:
        blog_posts = paginator.page(paginator.num_pages)

    return render(request, 'blog.html', {'blog_posts': blog_posts})


def blog_post(request, id):
    post = get_object_or_404(models.BlogPost, pk=id)
    related_posts = models.BlogPost.objects.all().order_by('-id')[:3]
    return render(request, 'blog-post.html', {'post': post, 'related': related_posts})
