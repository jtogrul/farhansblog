from django.contrib import admin
from .models import *
from image_cropping import ImageCroppingMixin


class ProjectPhotoInline(admin.StackedInline):
    model = ProjectPhoto


class ProjectAdmin(ImageCroppingMixin, admin.ModelAdmin):
    inlines = [ProjectPhotoInline, ]


class PortfolioPhotoAdmin(ImageCroppingMixin, admin.ModelAdmin):
    pass


class SlideAdmin(ImageCroppingMixin, admin.ModelAdmin):
    pass


class BlogPostAdmin(ImageCroppingMixin, admin.ModelAdmin):
    pass


class StoreItemVariantInline(admin.StackedInline):
    model = StoreItemVariant


class StoreItemAdmin(ImageCroppingMixin, admin.ModelAdmin):
    inlines = [StoreItemVariantInline, ]


class AboutSlideAdmin(ImageCroppingMixin, admin.ModelAdmin):
    ordering = ['order']


admin.site.register(PortfolioTheme)
admin.site.register(PortfolioPhoto, PortfolioPhotoAdmin)
admin.site.register(BlogPost, BlogPostAdmin)
admin.site.register(StoreItem, StoreItemAdmin)
admin.site.register(StoreTheme)
admin.site.register(Slide, SlideAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Order)
admin.site.register(AboutSlide, AboutSlideAdmin)
