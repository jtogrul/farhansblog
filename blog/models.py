from django.db import models
from django.utils import timezone
from image_cropping.fields import ImageRatioField
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from taggit.managers import TaggableManager


class PortfolioTheme(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class PortfolioPhoto(models.Model):
    name = models.CharField(max_length=100)
    desc = models.CharField(max_length=256)
    theme = models.ForeignKey(PortfolioTheme)
    image = models.ImageField(blank=True, upload_to='uploaded_photos')
    thumb = ImageRatioField('image', '520x400')
    thumb_about = ImageRatioField('image', '192x192')

    def __str__(self):
        return self.name


class Project(models.Model):
    name = models.CharField(max_length=100)
    desc = models.CharField(max_length=500)
    category = models.CharField(max_length=100)
    place = models.CharField(max_length=100)
    image = models.ImageField(blank=True, upload_to='uploaded_photos')
    thumb = ImageRatioField('image', '570x570')
    thumb_related = ImageRatioField('image', '639x500')
    thumb_arrow = ImageRatioField('image', '160x120')
    date_created = models.DateTimeField(default=timezone.now)
    date_published = models.DateTimeField(blank=True, null=True)
    date_shooted = models.DateTimeField(default=timezone.now)
    tags = TaggableManager()

    def __str__(self):
        return self.name


class ProjectPhoto(models.Model):
    name = models.CharField(max_length=100)
    desc = models.CharField(max_length=500)
    project = models.ForeignKey(Project)
    image = models.ImageField(blank=True, upload_to='uploaded_photos')

    def __str__(self):
        return self.name


class BlogPost(models.Model):

    SIZE_DEFAULT = 0
    SIZE_BIGGER_1 = 1
    SIZE_BIGGER_2 = 2

    SIZE_CHOICES = (
        (SIZE_DEFAULT, 'Default'),
        (SIZE_BIGGER_1, 'Bigger'),
        (SIZE_BIGGER_2, 'Even bigger')
    )

    title = models.CharField(max_length=200)
    category = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    body = RichTextUploadingField()
    size = models.SmallIntegerField(choices=SIZE_CHOICES, default=SIZE_DEFAULT, null=True, blank=True)
    date_created = models.DateTimeField(default=timezone.now)
    date_published = models.DateTimeField(blank=True, null=True)
    image = models.ImageField(blank=True, upload_to='uploaded_photos')
    hero = ImageRatioField('image', '1920x751')
    thumb_size_0 = ImageRatioField('image', '370x250')
    thumb_size_1 = ImageRatioField('image', '570x250')
    thumb_size_2 = ImageRatioField('image', '770x250')
    tags = TaggableManager()

    def publish(self):
        self.date_published = timezone.now()
        self.save()

    def get_size_class(self):
        if self.size is None:
            return ''
        else:
            return "size-%d" % self.size

    def get_thumb(self):
        return "thumb_size_%d" % self.size

    def __str__(self):
        return self.title


class Slide(models.Model):
    title = models.CharField(max_length=200)
    desc = models.CharField(max_length=500)
    button_text = models.CharField(max_length=50, blank=True, null=True)
    button_link = models.URLField(blank=True, null=True)
    image = models.ImageField(blank=True, upload_to='uploaded_photos')
    cropping = ImageRatioField('image', '1920x1000')

    def __str__(self):
        return self.title


class StoreTheme(models.Model):
    name = models.CharField(max_length=20)
    slug = models.SlugField(max_length=20)

    def __str__(self):
        return self.name


class StoreItem(models.Model):
    title = models.CharField(max_length=200)
    desc = models.CharField(max_length=500)
    theme = models.ForeignKey(StoreTheme)
    image = models.ImageField(upload_to='store_photos')
    thumb = ImageRatioField('image', '574x574')
    thumb_related = ImageRatioField('image', '479x300')
    tags = TaggableManager()

    def __str__(self):
        return self.title


class StoreItemVariant(models.Model):
    store_item = models.ForeignKey(StoreItem)
    title = models.CharField(max_length=50)
    file = models.FileField(upload_to='store_files')
    price = models.DecimalField(max_digits=6, decimal_places=2)
    contents = models.TextField(blank=True, null=True)

    def __str__(self):
        return "%s - %s" % (self.store_item.title, self.title)

    def price_in_cents(self):
        return int(self.price * 100)

    def buy_desc(self):
        return "Buy photo #%d (%s) from farhanhuseynli.com" % (int(self.store_item.id), self.title)


class Order(models.Model):
    item = models.ForeignKey(StoreItemVariant)
    amount = models.IntegerField()
    date_created = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=200)
    mail_sent = models.BooleanField(default=False)

    def __str__(self):
        return "%s - %s by %s (%s)" % (self.item.store_item.title, self.item.title, self.name, self.email)


class AboutSlide(models.Model):
    title = models.CharField(max_length=200)
    order = models.IntegerField()
    image = models.ImageField(blank=True, upload_to='uploaded_photos')
    cropping = ImageRatioField('image', '1140x855')
    ordering = ('-date',)

    def __str__(self):
        return self.title
