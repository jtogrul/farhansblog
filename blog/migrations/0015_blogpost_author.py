# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-26 13:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0014_auto_20170211_1325'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogpost',
            name='author',
            field=models.CharField(default='Farhan Huseynli', max_length=100),
            preserve_default=False,
        ),
    ]
