# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-22 10:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_auto_20170115_1416'),
    ]

    operations = [
        migrations.CreateModel(
            name='StoreItemVariant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('file', models.FileField(upload_to='store_files')),
                ('price', models.DecimalField(decimal_places=2, max_digits=6)),
                ('contents', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='storeitem',
            name='contents',
        ),
        migrations.RemoveField(
            model_name='storeitem',
            name='file',
        ),
        migrations.RemoveField(
            model_name='storeitem',
            name='price',
        ),
        migrations.AddField(
            model_name='storeitemvariant',
            name='store_item',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='blog.StoreItem'),
        ),
    ]
